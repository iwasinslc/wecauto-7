<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->string('confirmation_code')->nullable();
            $table->dateTime('confirmation_sent_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('transactions', ['confirmation_code', 'confirmation_sent_at']))
        {
            Schema::table('transactions', function (Blueprint $table) {
                $table->dropColumn('confirmation_code');
                $table->dropColumn('confirmation_sent_at');
            });
        }
    }
}
