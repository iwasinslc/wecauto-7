<?php


namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RequestDataTable;
use App\Http\Resources\OrderResource;
use App\Models\ExchangeOrder;

class OrdersController extends Controller
{
    /**
     * @OA\Get(
     *      path="/orders",
     *      operationId="getOrders",
     *      tags={"Orders"},
     *      summary="Get list of the orders",
     *      description="Return array of the orders. Filters: user_id",
     *      @OA\Parameter(
     *          ref="#/components/parameters/user_id"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OrderResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(RequestDataTable $request) {
        return OrderResource::collection(
                ExchangeOrder::with(['currency', 'mainCurrency'])
                    ->where('user_id', $request->user_id)
                    ->get()
            );
    }

}