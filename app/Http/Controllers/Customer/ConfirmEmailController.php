<?php
namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\User;

class ConfirmEmailController extends Controller
{
    /**
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(string $hash)
    {
        /** @var User $findUserViaHash */
        $findUserViaHash = User::where('email_verification_hash', $hash)
            ->whereNull('email_verified_at')
            ->first();

        if (null !== $findUserViaHash) {
            if($findUserViaHash->email_verification_sent->diffInMinutes(now()) > 30) {
                $message = 'Verification link has been expired';
            } else {
                $findUserViaHash->email_verified_at = now()->toDateTimeString();
                $findUserViaHash->save();
                $message = 'Your email has been confirmed';
            }
        } else {
            $message = 'Your email can not be confirmed';
        }
        return view('customer.message_block', [
            'message' => $message
        ]);
    }

    /**
     * Send verification email to user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function send_verification_email() {
        $user = user();
        if($user->isVerifiedEmail()) {
            return back()->with('error', __('Email already verified'));
        }

        if(!$user->canSendVerificationEmail()) {
            return back()->with('error', __('Email can be sent only once per minute'));
        }

        if($user->sendVerificationEmail()) {
            return back()->with('success', __('Message was sent. Please check you email box!'));
        } else {
            return back()->with('error', __('Verification email currently not available'));
        }
    }
}
