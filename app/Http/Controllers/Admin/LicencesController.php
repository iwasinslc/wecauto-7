<?php
namespace App\Http\Controllers\Admin;


use App\Models\Licences;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class ProjectsController
 * @package App\Http\Controllers\Admin
 */
class LicencesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $licences = Licences::orderBy('id')->get();

        return view('admin.licences.index', [
            'licences' => $licences,
        ]);
    }



    public function statistic()
    {
        $licences = Licences::orderBy('id')->get();

        return view('admin.licences.statistic', [
            'licences' => $licences,
        ]);
    }

    /**
     * @param string $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $id)
    {
        /** @var Licences $licence */
        $licence = Licences::find($id);

        if (null == $licence) {
            abort(404);
        }

        return view('admin.licences.edit', [
            'licence' => $licence,
        ]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.licences.create');
    }

    /**
     * @param Request $request
     * @return $this|RedirectResponse
     */
    public function store(Request $request)
    {
        $licence = new Licences();

        if (!$licence) {
            return back()->with('error', __('Unable to create tariff plan'))->withInput();
        }

        $licence->name                  = strip_tags($request->name);
        $licence->duration      = strip_tags($request->duration);
        $licence->price      = strip_tags($request->price);
        $licence->currency_id      = strip_tags($request->currency_id);
        $licence->buy_amount      = strip_tags($request->buy_amount);
        $licence->sell_amount      = strip_tags($request->sell_amount);
        $licence->save();

        return redirect()->route('admin.licences.index')->with('success', __('Licence plan has been created'));
    }


    /**
     * @param \Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, string $id)
    {
        /** @var Licences $licence */
        $licence = Licences::find($id);

        if (null == $licence) {
            abort(404);
        }



        $licence->name                  = strip_tags($request->name);
        $licence->duration      = strip_tags($request->duration);
        $licence->price      = strip_tags($request->price);
        $licence->currency_id      = strip_tags($request->currency_id);
        $licence->buy_amount      = strip_tags($request->buy_amount);
        $licence->sell_amount      = strip_tags($request->sell_amount);



        $licence->save();

        return back()->with('success', 'Лицензия успешно обновлена')->withInput();
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(string $id)
    {
        /** @var Licences $licence */
        $licence = Licences::find($id);

        if (null == $licence) {
            abort(404);
        }

        if ($licence->delete()) {
            return back()->with('success', 'Лицензия успешно удалена');
        }
        return back()->with('error', 'Лицензия не может быть удален');
    }
}
