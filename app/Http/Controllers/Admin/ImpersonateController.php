<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Support\Facades\Auth;

/**
 * Class ImpersonateController
 * @package App\Http\Controllers\Admin
 */
class ImpersonateController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function impersonate($id)
    {
        $user = User::find($id);

        if (null == $user) {
            return back()->with('error', __('User not found'))->withInput();
        }

        /*
         * Register wallets
         */
        $paymentSystems = PaymentSystem::all();

        /** @var PaymentSystem $paymentSystem */
        foreach ($paymentSystems as $paymentSystem) {
            $currencies = $paymentSystem->currencies()->get();

            /** @var Currency $currency */
            foreach ($currencies as $currency) {
                $checkWallet = $user->wallets()
                    ->where('currency_id', $currency->id)
                    ->count();

                if (!$checkWallet) {
                    Wallet::newWallet($user, $currency, $paymentSystem);
                }
            }
        }

        Auth::user()->impersonate($user);
        return redirect(route('profile.profile'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function leave()
    {
        Auth::user()->leaveImpersonation();
        return redirect(route('admin'));
    }
}
