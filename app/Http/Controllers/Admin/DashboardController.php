<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        if (!user()->hasRole('root'))
        {
            return redirect()->route('admin.news.index');
        }
        return view('admin.dashboard');
    }
}
