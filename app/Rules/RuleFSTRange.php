<?php
namespace App\Rules;

use App\Models\Deposit;
use Illuminate\Contracts\Validation\Rule;
use App\Models\Rate;

/**
 * Class RulePlanRange
 * @package App\Rules
 */
class RuleFSTRange implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  float  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /**
         * @var Deposit $deposit
         */
        $deposit = Deposit::find(request()->deposit_id);

        if ($deposit===null) {
            return false;
        }

//      $days = $deposit->maxDays();
//      return $value <= $days;

        return $value >= $deposit->minNumberDays() &&  $value < $deposit->maxNumberDays();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The amount is not included in the license limitation');
    }
}
