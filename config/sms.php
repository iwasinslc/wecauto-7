<?php
return [
    'set_twilio_number' => env('SET_TWILIO_NUMBER'),
    'set_twilio_sid' => env('SET_TWILIO_SID'),
    'set_twilio_token' => env('SET_TWILIO_TOKEN'),
];
