@extends('layouts.profile')
@section('title', __('Cars'))
@section('content')
    <section>
        <div class="container">
            <div class="car-orders">
                <h3 class="lk-title">Car orders
                </h3>
                @foreach($deposits as $deposit)
                    <div class="vehicle-item {{$deposit->type==1 ? 'vehicle-item--type-bike' : ''}}">
                        <div class="vehicle-item__content">
                            <div class="vehicle-item__top">
                                <p class="order-card__title"><strong>{{$deposit->name}}</strong></p>
                                <div class="clock-action">
                                    <div class="clock-action__top">
                                        <svg class="svg-icon">
                                            <use href="/assets/icons/sprite.svg#icon-wall-clock"></use>
                                        </svg>
                                        <p>{{now()->diffInDays($deposit->datetime_closing)}} left</p>
                                    </div>
                                </div>
                            </div>
                            <div class="v-indicator">
                                <div class="v-indicator__bar"><span style="width:{{$deposit->progressPercentage()}}%"><span>{{now()->format('d/m')}}<span>‘{{now()->format('y')}}</span></span></span>
                                </div>
                                <p class="v-indicator__start-date">{{$deposit->created_at->format('d/m')}} <span>‘{{$deposit->created_at->format('y')}}</span>
                                </p>
                                <p class="v-indicator__end-date">{{$deposit->datetime_closing->format('d/m')}} <span>‘{{$deposit->datetime_closing->format('y')}}</span>
                                </p>
                            </div>
                        </div>
                        <div class="vehicle-item__price">
                            <p class="order-card-top__price">{{number_format($deposit->balance, 4)}} WEC</p>
                            <div class="vehicle-item__buttons">
                                <!---->
                                @if ($deposit->fst_in_usd>0)
                                    <p class="order-card__fst"><img src="/assets/images/fst-white2.png" alt=""><span>${{number_format($deposit->fst_in_usd, 2)}}</span>
                                    </p>
                            @endif
                            @if ($deposit->allowedExecutionDays())
                                <a class="apply-fst" href="{{route('profile.deposits.add_fst', ['id'=>$deposit->id])}}"><i>{{__('Apply')}} </i><img src="/assets/images/fst2.png" alt=""></a>
                            @endif
                            </div>


                        </div>
                        <div class="vehicle-item__image"><img src="{{getPhotoPath($deposit->image)}}" alt="">
                        </div>
                    </div>
                @endforeach

{{--                <div class="vehicle-item vehicle-item--type-bike">--}}
{{--                    <div class="vehicle-item__content">--}}
{{--                        <div class="vehicle-item__top">--}}
{{--                            <p class="order-card__title"><strong>BMW K1000</strong> <span class="color-warning">/</span> 2020 0.8L</p>--}}
{{--                            <div class="clock-action">--}}
{{--                                <div class="clock-action__top">--}}
{{--                                    <svg class="svg-icon">--}}
{{--                                        <use href="/assets/icons/sprite.svg#icon-wall-clock"></use>--}}
{{--                                    </svg>--}}
{{--                                    <p>11m 10d left</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="v-indicator">--}}
{{--                            <div class="v-indicator__bar"><span style="width:60%"><span>15/03 <span>‘20</span></span></span>--}}
{{--                            </div>--}}
{{--                            <p class="v-indicator__start-date">22/02 <span>‘20</span>--}}
{{--                            </p>--}}
{{--                            <p class="v-indicator__end-date">22/03 <span>‘20</span>--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="vehicle-item__price">--}}
{{--                        <p class="order-card-top__price">12.121 WEC</p>--}}
{{--                        <p class="order-card-top__price color-warning"> <i>342 FST</i></p>--}}
{{--                        <div class="fst fst--line-top">--}}
{{--                            <p>Fast your speed with</p><img src="/assets/images/fst2.png" alt="">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="vehicle-item__image"><img src="/assets/images/orders/2.png" alt="">--}}
{{--                    </div>--}}
{{--                </div>--}}
                <!-- Add--><a class="add-vehicle" href="{{route('profile.deposits.create')}}">
                    <div class="order-card__add"><span>{!! __('Add another vehicle') !!}</span></div><span class="add-vehicle__image"><img src="/assets/images/orders/car.png" alt=""></span></a>
            </div>
        </div>
    </section>
    <!-- /.card -->
@endsection

@push('load-scripts')

@endpush
