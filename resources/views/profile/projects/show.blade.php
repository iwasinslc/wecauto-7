@extends('layouts.profile')
@section('title', 'Проект «'.$project->name.'»')
@section('content')
    <!-- Проектная карточка -->
    <style>
        .card_image {
            height: 200px;
            overflow: hidden;
        }

        .card-text {
            font-size: 12px;
            color: #333;
        }

        .card_title {
            margin-bottom: 10px;
            display: block;
        }

        .badge {
            padding: 3px 10px;
        }

        .form-control,
        .input-group-text {
            border-radius: 0;
        }
    </style>
    <div class="pr_card">
        <div class="row">
            <div class="col-lg-12">
                @include('partials.inform')
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="/project_img/{{ $project->logotype_url }}" class="card-img" alt="..." style="max-width:300px;">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Проект «{{ $project->name }}»:</h5>
                                <p class="card-text">
                                    {{ $project->description }}
                                </p>
                                <p class="card-text">
                                    <small class="text-muted">Название проекта</small>
                                    <span class="badge badge-default">{{ $project->name }}</span>
                                </p>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"></li>
                                <li class="list-group-item">
                                    <h5 class="card-title">Маркетинг план</h5>
                                    <p class="card-text">
                                        {{ $project->marketing_description }}
                                    </p>
                                </li>
                                <li class="list-group-item">Контакты куратора (имя, телеграм логин)
                                    <?php
                                    preg_match('/\@.+/', $project->curator_contacts, $telegram);
                                    ?>
                                    <span class="badge badge-success ml-3">{{ preg_replace('/\@.+/', '', $project->curator_contacts) }}</span>
                                    @if(isset($telegram[0]))
                                        <span class="badge badge-warning"><a href="https://teleg.run/{{ $telegram[0] }}" target="_blank">{{ $telegram[0] }}</a></span>
                                    @endif
                                </li>
                                <li class="list-group-item text-justify">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon3">Ссылка на проект:</span>
                                        </div>
                                        <input type="text" class="form-control text-muted" id="basic-url" aria-describedby="basic-addon3" value="{{ $project->url }}">
                                    </div>
                                </li>

                                @if(!empty($project->video_description))
                                <li class="list-group-item text-justify" style="width:516px;">
                                    <div class="mb-2">Видео</div>
                                    <?php
                                    $videoUrl = str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $project->video_description);
                                    ?>
                                    <div class="row">
                                        <iframe width="516" height="315" src="{{ $videoUrl }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>

                                    <div class="row" style="margin-top:15px; text-align: center;">
                                        <div class="col-lg-6">
                                            <div>Количество лайков</div> <span class="badge badge-success badge-pill">{{ \App\Models\ProjectLikes::where('project_id', $project->id)->where('is_like', 1)->count() }}</span>
                                            <div>
                                                <a href="{{ route('profile.projects.like', ['id' => $project->id]) }}">
                                                    <button type="button" class="btn btn-success mt-3">Поставить лайк</button>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div>Количество дизлайков</div> <span class="badge badge-warning badge-pill">{{ \App\Models\ProjectLikes::where('project_id', $project->id)->where('is_like', 0)->count() }}</span>
                                            <div>
                                                <a href="{{ route('profile.projects.dislike', ['id' => $project->id]) }}">
                                                    <button type="button" class="btn btn-warning mt-3">Поставить дизлайн</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endif

{{--                                <li class="list-group-item text-justify">--}}
{{--                                    <div class="row text-center">--}}
{{--                                        --}}
{{--                                    </div>--}}
{{--                                </li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Проектная карточка -->
@endsection