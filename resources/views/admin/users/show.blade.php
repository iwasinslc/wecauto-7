@extends('admin/layouts.app')
@section('title'){{ __('User profile') }}: {{ $user->name }}@endsection
@section('breadcrumbs')
    <li><a href="{{route('admin.users.index')}}">{{ __('Users list') }}</a></li>
    <li> {{ __('User profile') }}: {{ $user->login }}</li>
@endsection
@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('User profile') }}</h1>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Name:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $user->name }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('E-mail:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    <a href="mailto:{{ $user->email }}" target="_blank">{{ $user->email }}</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Login:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $user->login }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Phone:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    <a href="tel:{{ $user->phone }}" target="_blank">{{ $user->phone }}</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Skype:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    <a href="skype:{{ $user->skype }}" target="_blank">{{ $user->skype }}</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Partner ID:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    @if($partner = $user->getPartnerOnLevel(1))
                                    <a href="{{ route('admin.users.show', ['user'=>$partner->id]) ?? '' }}"
                                       target="_blank">{{ $partner->login }}</a>
                                    @endif
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Registration:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    <strong data-toggle="tooltip" data-placement="top"
                                            title="{{ \Carbon\Carbon::parse($user->created_at)->diffForHumans() }}">{{ $user->created_at }}</strong>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Rank') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                   {{$user->rank->name}}
                                </div>
                            </div>
                        </li>


                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Licence') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{$user->activeLicence() ? $user->licence->price : ''}}
                                </div>
                            </div>
                        </li>


                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Sell Limit') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $user->sell_limit}}
                                </div>
                            </div>
                        </li>


                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Buy Limit') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $user->buy_limit}}
                                </div>
                            </div>
                        </li>


                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Куплено FST') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $user->orderPieces()->where('type', \App\Models\ExchangeOrder::TYPE_BUY)->where('main_currency_id', \App\Models\Currency::getByCode('FST')->id)->sum('amount')}}
                                </div>
                            </div>
                        </li>


                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Продано FST') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $user->orderPieces()->where('main_currency_id', \App\Models\Currency::getByCode('FST')->id)->where('type', \App\Models\ExchangeOrder::TYPE_SELL)->sum('amount')}}
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Куплено WEC') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $user->orderPieces()->where('type', \App\Models\ExchangeOrder::TYPE_BUY)->where('main_currency_id', \App\Models\Currency::getByCode('WEC')->id)->sum('amount')}}
                                </div>
                            </div>
                        </li>


                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Продано WEC') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $user->orderPieces()->where('main_currency_id', \App\Models\Currency::getByCode('WEC')->id)->where('type', \App\Models\ExchangeOrder::TYPE_SELL)->sum('amount')}}
                                </div>
                            </div>
                        </li>



                        @foreach (getCurrencies() as $currency)
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-5">
                                        {{ __('Попоплнено') }} {{$currency['code']}}:
                                    </div>
                                    <div class="col-lg-7" style="font-weight:bold;">
                                        <strong>{{ getUserTotalByTransactionType('enter', $user)[$currency['code']]}}</strong>
                                    </div>
                                </div>
                            </li>


                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-5">
                                        {{ __('Выведено') }} {{$currency['code']}}:
                                    </div>
                                    <div class="col-lg-7" style="font-weight:bold;">
                                        <strong>{{ getUserTotalByTransactionType(\App\Models\TransactionType::TRANSACTION_TYPE_WITHDRAW, $user)[$currency['code']] }}</strong>
                                    </div>
                                </div>
                            </li>
                        @endforeach

                    </ul>
                    <strong>{{ __('Referral link:') }}</strong>
                    <input type="text" class="form-control" value="{{ route('partner',['partner_id'=>$user->my_id]) }}"
                           readonly>
                    <hr>
                    <a href="{{ route('admin.users.edit', ['user' => $user->id]) }}" target="_top"
                       class="btn btn-primary btn-large" style="display: block;">{{ __('Edit user') }}</a>
                    <a href="{{ route('admin.users.reset2fa', ['id' => $user->id]) }}" style="margin-top:3px; width:100%;" target="_top"
                       class="btn btn-default btn-large" style="display: block;">{{ __('Reset 2Fa') }}</a>

                    <a href="{{ route('admin.impersonate', ['id' => $user->id]) }}" style="margin-top:3px; width:100%;" target="_top"
                       class="btn btn-default btn-large" style="display: block;">{{ __('Impersonate user') }}</a>
                    <form action="{{ route('admin.users.destroy', ['user' => $user->id]) }}" method="POST" target="_top">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" class="btn btn-danger btn-large sure form-control"
                               style="display: block; margin-top:5px;" value="{{ __('Destroy user') }}">
                    </form>
                    <hr>
                    @if ($user->getRoleNames()->count() > 0)
                        <p>{{ __('Roles') }}:
                            @foreach($user->getRoleNames() as $role)
                                <strong>{{ $role }}</strong>{{ !$loop->last ? ', ' : '' }}
                            @endforeach
                        </p>
                    @endif
                </div>
                <!-- /tile body -->
            </section>
            <!-- /tile -->
        </div>
        <div class="col-md-12">
            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Wallets') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" tabindex="0" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <table id="wallets" class="dataTables_wrapper hover form-inline dt-bootstrap no-footer">
                        <thead>
                        <tr>
                            <th>{{ __('Balance') }}</th>
                            <th>{{ __('Currency') }}</th>
                            <th>{{ __('Адрес пополнения') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->wallets()->with(['currency', 'paymentSystem'])->get() as $wallet)
                            @if ($wallet->currency->code!=='USD'||$wallet->paymentSystem->code=='perfectmoney')
                                <tr>
                                    <td><strong data-toggle="tooltip" data-placement="top"
                                                title="{{ __('Current balance') }}">{{ $wallet->balance }}{{$wallet->currency->symbol}}</strong>
                                        @if ($wallet->requestedAmount())
                                            <br> <strong class="help-block" data-toggle="tooltip" data-placement="left"
                                                         title="{{ __('Requested amount') }}">{{ $wallet->requestedAmount() }}{{ $wallet->currency->symbol }}</strong>
                                        @endif
                                    </td>
                                    <td><a href="{{ route('admin.currencies.edit', ['currency' => $wallet->currency->id]) }}"
                                           target="_blank">{{ $wallet->currency->name }}</a></td>

                                    <td>{{$wallet->address}}</td>
                                    <td>
                                        <button class="btn btn-default" data-toggle="modal" data-target="#bonusModal"
                                                data-wallet-id="{{ $wallet->id }}" type="button">
                                            {{ __('main.bonus.send') }}
                                        </button>
                                        <form class="form-horizontal" method="POST"
                                              action="{{ route('admin.users.penalty') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="wallet_id" value="{{ $wallet->id }}">
                                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                                            <fieldset>
                                                <div class="form-group">
                                                    <div class="col-sm-10">
                                                        <div class="input-group mb-10">
                                                    <span class="input-group-btn">
                                                         <button class="btn btn-default"
                                                                 type="submit">{{ __('main.penalty.send') }}</button>
                                                    </span>
                                                            <input id="amount" type="number" step="any" min="0"
                                                                   class="form-control"
                                                                   name="amount" placeholder="{{ __('main.amount') }}" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </td>
                                </tr>
                            @endif

                        @endforeach
                        </tbody>
                    </table>

                </div>
                <!-- /tile body -->
            </section>

            @push('load-scripts')
                <script>
                    $('#wallets').DataTable();
                </script>
        @endpush
        <!-- /tile -->
        </div>

        <div class="col-md-12">
            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Withdraw requests') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <div class="table-responsive">
                        <form method="POST"
                              action="{{ route('admin.requests.approve-many') }}">
                            {{ csrf_field() }}
                            <table class="table table-custom" id="wrs-table">
                                <thead>
                                <tr>
                                    <th>{{ __('Date') }}</th>
                                    <th>{{ __('Payment system') }}</th>
                                    <th>{{ __('Currency') }}</th>
                                    <th>{{ __('Amount') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Actions') }}</th>

                                </tr>
                                </thead>
                                <tfoot>
                                <style>
                                    td.tdinput input {
                                        width: 100%;
                                    }
                                </style>
                                <tr>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput">
                                        <b>{{ __('selected orders') }}:</b>
                                        <button id="singlebutton" name="approve" value="true"
                                                class="btn btn-xs btn-primary">{{ __('approve') }}</button>
                                        <button id="singlebutton" name="reject" value="true"
                                                class="btn btn-xs btn-warning">{{ __('reject') }}</button>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>
                    </div>
                </div>
                <!-- /tile body -->

            </section>

            @push('load-scripts')
                <script>
                    var table = $('#wrs-table').width('100%').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "ajax": '{{route('admin.users.dt-wrs',['user_id'=>$user->id])}}',
                        "columns": [
                            {"data": "created_at", "name": "withdraws.created_at"},
                            {"data": "payment_system.name", "name": "paymentSystem.name"},
                            {"data": "currency.code", "name": "currency.code"},
                            {"data": "amount", "name": "withdraws.amount"},
                            {"data": "status", "name": "withdraws.status"},
                            {
                                "data": 'actions',
                                "orderable": false,
                                "searchable": false,
                                "render": function (data, type, row) {
                                    return '<input type="checkbox" name="list[]" value="' + row['id'] + '"> &nbsp; <a href="/admin/requests/' + row['id'] + '" class="btn btn-xs btn-primary" target="_blank"><i class="glyphicon glyphicon-eye-open"></i> {{ __('show') }}</a> &nbsp; <a href="/admin/requests/approve/' + row['id'] + '" class="btn btn-xs btn-success" target="_blank"><i class="glyphicon glyphicon-check"></i> {{ __('approve') }}</a> &nbsp;<a href="/admin/requests/reject/' + row['id'] + '" class="btn btn-xs btn-warning" target="_blank"><i class="glyphicon glyphicon-check"></i> {{ __('reject') }}</a>';
                                }
                            },
                        ],
                        "aoColumnDefs": [
                            {'bSortable': false, 'aTargets': ["no-sort"]}
                        ],
                        "dom": 'Rlfrtip',
                        initComplete: function () {
                            this.api().columns([0, 1, 2, 3]).every(function () {
                                var column = this;
                                var input = document.createElement("input");
                                $(input).appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        column.search($(this).val(), false, false, true).draw();
                                    });
                            });
                        }
                    });

                    $('#wrs-table tbody').on('click', 'tr', function () {
                        if ($(this).hasClass('row_selected')) {
                            $(this).removeClass('row_selected');
                        }
                        else {
                            table.$('tr.row_selected').removeClass('row_selected');
                            $(this).addClass('row_selected');
                        }
                    });
                </script>
            @endpush
        <!-- /tile -->


            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Transactions') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-custom" id="transactions-table">
                            <thead>
                            <tr>
                                <th>{{ __('Type') }}</th>
                                <th>{{ __('Currency') }}</th>
                                <th>{{ __('Amount') }}</th>
                                <th>{{ __('Approved') }}</th>
                                <th>{{ __('Created') }}</th>
                                <th>{{ __('Action') }}</th>

                            </tr>
                            </thead>
                            <tfoot>
                            <style>
                                td.tdinput input {
                                    width: 100%;
                                }
                            </style>
                            <tr>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /tile body -->
            </section>
            @push('load-scripts')
                <script>
                    //initialize basic datatable
                    var table = $('#transactions-table').width('100%').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[4, "desc"]],
                        "ajax": '{{route('admin.users.dt-transactions',['user_id'=>$user->id])}}',
                        "columns": [
                            {"data": "type_name"},
                            {"data": "currency.code"},
                            {"data": "amount"},
                            {"data": "approved"},
                            {"data": "created_at"},
                            {
                                "data": 'actions',
                                "orderable": false,
                                "searchable": false,
                                "render": function (data, type, row) {
                                    return '<a href="/admin/transactions/' + row['id'] + '" class="btn btn-xs btn-primary" target="_blank"><i class="glyphicon glyphicon-eye-open"></i> {{ __('show') }}</a>';
                                }
                            },
                        ],
                        "aoColumnDefs": [
                            {'bSortable': false, 'aTargets': ["no-sort"]}
                        ],
                        "dom": 'Rlfrtip',
                        initComplete: function () {
                            this.api().columns([0, 1, 2, 3, 4]).every(function () {
                                var column = this;
                                var input = document.createElement("input");
                                $(input).appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        column.search($(this).val(), false, false, true).draw();
                                    });
                            });
                        }
                    });

                    $('#transactions-table tbody').on('click', 'tr', function () {
                        if ($(this).hasClass('row_selected')) {
                            $(this).removeClass('row_selected');
                        }
                        else {
                            table.$('tr.row_selected').removeClass('row_selected');
                            $(this).addClass('row_selected');
                        }
                    });
                    // *initialize basic datatable
                </script>
            @endpush
            <!-- /tile -->


            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font"> {{ __('Deposits') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-custom" id="deposits-table">
                            <thead>
                            <tr>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Currency') }}</th>
                                <th>{{ __('Invested') }}</th>
                                <th>{{ __('Duration days') }}</th>
                                <th>{{ __('Дата закрытия') }}</th>
                                <th>{{ __('Opened') }}</th>
                                <th>{{ __('Actions') }}</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <style>
                                td.tdinput input {
                                    width: 100%;
                                }
                            </style>
                            <tr>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /tile body -->
            </section>
            @push('load-scripts')
                <script>
                    var table = $('#deposits-table').width('100%').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[5, "desc"]],
                        "ajax": '{{route('admin.users.dt-deposits',['user_id'=>$user->id])}}',
                        "columns": [
                            {"data": "condition"},
                            {"data": "currency.code"},
                            {"data": "invested"},
                            {"data": "duration"},
                            {"data": "datetime_closing"},
                            {"data": "created_at"},
                            {
                                "data": 'actions',
                                "orderable": false,
                                "searchable": false,
                                "render": function (data, type, row) {
                                    return '<a href="/admin/deposits/' + row['id'] + '" class="btn btn-xs btn-primary"  target="_blank"><i class="glyphicon glyphicon-eye-open"></i> {{ __('show') }}</a>';
                                }
                            },
                        ],
                        "aoColumnDefs": [
                            {'bSortable': false, 'aTargets': ["no-sort"]}
                        ],
                        "dom": 'Rlfrtip',
                        initComplete: function () {
                            this.api().columns([0, 1, 2, 3, 4, 5]).every(function () {
                                var column = this;
                                var input = document.createElement("input");
                                $(input).appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        column.search($(this).val(), false, false, true).draw();
                                    });
                            });
                        }
                    });

                    $('#deposits-table tbody').on('click', 'tr', function () {
                        if ($(this).hasClass('row_selected')) {
                            $(this).removeClass('row_selected');
                        }
                        else {
                            table.$('tr.row_selected').removeClass('row_selected');
                            $(this).addClass('row_selected');
                        }
                    });
                </script>
            @endpush
        <!-- /tile -->


            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Orders') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-custom" id="orders-table">
                            <thead>
                            <tr>
                                <th>{{__('Time')}}</th>
                                <th>{{__('Time Update')}}</th>
                                <th>{{ __('Type') }}</th>
                                <th>{{ __('Amount') }}</th>
                                <th>{{ __('Price') }}</th>
                                <th>{{ __('Volume') }}</th>

                                <th>{{ __('Licence Volume') }}</th>
                                <th>{{__('Статус')}}</th>

                            </tr>
                            </thead>
                            <tfoot>
                            <style>
                                td.tdinput input {
                                    width: 100%;
                                }
                            </style>
                            <tr>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /tile body -->
            </section>
            @push('load-scripts')
                <script>
                    //initialize basic datatable
                    var table = $('#orders-table').width('100%').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[0, "desc"]],
                        "ajax": '{{route('admin.users.dt-orders',['user_id'=>$user->id])}}',
                        "columns": [
                            {
                                "data": "created_at", "render": function (data, type, row, meta) {
                                    if (row['type'] == 0) {
                                        return '<div class="time-indicator time-indicator--sale"><span>' + row['created_at'] + '</span></div>';
                                    }
                                    return '<div class="time-indicator time-indicator--buy"><span>' + row['created_at'] + '</span></div>';
                                }
                            },
                            {"data": "updated_at"},
                            {
                                "data": "type", "render": function (data, type, row, meta) {
                                    if (row['type'] == 0) {
                                        return '{{ __('Sell') }}';
                                    }
                                    return '{{ __('Buy') }}';
                                }
                            },
                            {
                                "data": 'amount',
                                "orderable": true,
                                "searchable": true,
                                "render": function (data, type, row, meta) {
                                    return row['amount'] +' '+ row['main_currency']['code'];
                                }
                            },
                            {
                                "data": 'rate',
                                "orderable": true,
                                "searchable": true,
                                "render": function (data, type, row, meta) {
                                    return row['rate']+' '+ row['currency']['code'];
                                }
                            },

                            {
                                "data": 'rate_amount',
                                "orderable": true,
                                "searchable": true,
                                "render": function (data, type, row, meta) {
                                    return row['amount']*row['rate'] +' '+ row['currency']['code'];
                                }
                            },



                            {
                                "data": 'amount',
                                "orderable": true,
                                "searchable": true,
                                "render": function (data, type, row, meta) {
                                    return row['amount']*row['licence_rate'] +' USD';
                                }
                            },

                            {
                                "data": 'closed',
                                "orderable": true,
                                "searchable": true,
                                "render": function (data, type, row, meta) {
                                    if(row['closed']==1)
                                    {
                                        return 'Закрыт Пользователем'
                                    }

                                    if(row['active']==0)
                                    {
                                        return 'Исполнен'
                                    }
                                    return 'Активен';
                                }
                            },


                        ],
                        "aoColumnDefs": [
                            {'bSortable': false, 'aTargets': ["no-sort"]}
                        ],
                        "dom": 'Rlfrtip',
                        initComplete: function () {
                            this.api().columns([0, 1, 2, 3, 5]).every(function () {
                                var column = this;
                                var input = document.createElement("input");
                                $(input).appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        column.search($(this).val(), false, false, true).draw();
                                    });
                            });
                        }
                    });

                    $('#transactions-table tbody').on('click', 'tr', function () {
                        if ($(this).hasClass('row_selected')) {
                            $(this).removeClass('row_selected');
                        }
                        else {
                            table.$('tr.row_selected').removeClass('row_selected');
                            $(this).addClass('row_selected');
                        }
                    });
                    // *initialize basic datatable
                </script>
            @endpush

            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Trades') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-custom" id="trades-table">
                            <thead>
                            <tr>
                                <th>{{__('Time')}}</th>
                                <th>{{ __('Type') }}</th>
                                <th>{{ __('Amount') }}</th>
                                <th>{{ __('Price') }}</th>
                                <th>{{ __('Volume') }}</th>

                                <th>{{ __('Licence Volume') }}</th>
                                <th>{{__('Fee')}}</th>

                            </tr>
                            </thead>
                            <tfoot>
                            <style>
                                td.tdinput input {
                                    width: 100%;
                                }
                            </style>
                            <tr>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /tile body -->
            </section>
            @push('load-scripts')
                <script>
                    //initialize basic datatable
                    var table = $('#trades-table').width('100%').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[0, "desc"]],
                        "ajax": '{{route('admin.users.dt-trades',['user_id'=>$user->id])}}',
                        "columns": [
                            {
                                "data": "created_at", "render": function (data, type, row, meta) {
                                    if (row['type'] == 0) {
                                        return '<div class="time-indicator time-indicator--sale"><span>' + row['created_at'] + '</span></div>';
                                    }
                                    return '<div class="time-indicator time-indicator--buy"><span>' + row['created_at'] + '</span></div>';
                                }
                            },
                            {
                                "data": "type", "render": function (data, type, row, meta) {
                                    if (row['type'] == 0) {
                                        return '{{ __('Sell') }}';
                                    }
                                    return '{{ __('Buy') }}';
                                }
                            },
                            {
                                "data": 'amount',
                                "orderable": true,
                                "searchable": true,
                                "render": function (data, type, row, meta) {
                                    return row['amount'] +' '+ row['main_currency']['code'];
                                }
                            },
                            {
                                "data": 'rate',
                                "orderable": true,
                                "searchable": true,
                                "render": function (data, type, row, meta) {
                                    return row['rate']+' '+ row['currency']['code'];
                                }
                            },

                            {
                                "data": 'rate_amount',
                                "orderable": true,
                                "searchable": true,
                                "render": function (data, type, row, meta) {
                                    return row['rate_amount'] +' '+ row['currency']['code'];
                                }
                            },



                            {
                                "data": 'order.amount',
                                "orderable": true,
                                "searchable": true,
                                "render": function (data, type, row, meta) {
                                    return row['limit_amount'] +' USD';
                                }
                            },

                            {"data": "fee"},
                        ],
                        "aoColumnDefs": [
                            {'bSortable': false, 'aTargets': ["no-sort"]}
                        ],
                        "dom": 'Rlfrtip',
                        initComplete: function () {
                            this.api().columns([0, 1, 2, 3, 5]).every(function () {
                                var column = this;
                                var input = document.createElement("input");
                                $(input).appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        column.search($(this).val(), false, false, true).draw();
                                    });
                            });
                        }
                    });

                    $('#transactions-table tbody').on('click', 'tr', function () {
                        if ($(this).hasClass('row_selected')) {
                            $(this).removeClass('row_selected');
                        }
                        else {
                            table.$('tr.row_selected').removeClass('row_selected');
                            $(this).addClass('row_selected');
                        }
                    });
                    // *initialize basic datatable
                </script>
            @endpush
        <!-- /tile -->

            <!-- /tile -->

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Registered page requests') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-custom" id="pvs-table">
                            <thead>
                            <tr>
                                <th>{{ __('IP address') }}</th>
                                <th>{{ __('Location') }}</th>
                                <th>{{ __('Request address') }}</th>
                                <th>{{ __('Date') }}</th>
                            </tr>
                            </thead>
                            <style>
                                #pvs-table tbody td div.page_url {
                                    width: 400px;
                                    overflow: auto;
                                }
                            </style>
                            <tfoot>
                            <style>
                                td.tdinput input {
                                    width: 100%;
                                }
                            </style>
                            <tr>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /tile body -->
            </section>

            @push('load-scripts')
                <script>
                    var table = $('#pvs-table').width('100%').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[2, "desc"]],
                        "ajax": '{{route('admin.users.dt-pvs',['user_id'=>$user->id])}}',
                        "columns": [
                            {"data": "user_ip"},
                            {"data": "location", "orderable": false, "searchable": false},
                            {
                                "data": "page_url", "render": function (data, type, row) {
                                    return '<textarea class="form-control" readonly>' + row['page_url'] + '</textarea>';
                                }
                            },
                            {"data": "created_at"},
                        ],
                        "aoColumnDefs": [
                            {'bSortable': false, 'aTargets': ["no-sort"]}
                        ],
                        "dom": 'Rlfrtip',
                        initComplete: function () {
                            this.api().columns([0, 2, 3]).every(function () {
                                var column = this;
                                var input = document.createElement("input");
                                $(input).appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        column.search($(this).val(), false, false, true).draw();
                                    });
                            });
                        }
                    });

                    $('#pvs-table tbody').on('click', 'tr', function () {
                        if ($(this).hasClass('row_selected')) {
                            $(this).removeClass('row_selected');
                        }
                        else {
                            table.$('tr.row_selected').removeClass('row_selected');
                            $(this).addClass('row_selected');
                        }
                    });
                </script>
        @endpush
        <!-- /tile -->

        </div>
        <!-- /col -->
    </div>
    <!-- /row -->

@endsection

@section('modal')
    <div class="modal fade" id="bonusModal" tabindex="-1" role="dialog" aria-labelledby="bonusModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('main.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title text-center" id="bonusModalLabel">{{ __('main.bonus.send') }}</h5>
                </div>
                <div class="modal-body">
                    <form id="bonusModalForm" style="padding: 0 15px;" class="form-horizontal" method="POST" action="{{ route('admin.users.bonus') }}">
                        @csrf
                        <div class="form-group">
                            <label for="bonusModalAmountInput">{{ __('main.amount') }}</label>
                            <input id="bonusModalAmountInput" type="number" step="any" min="0" class="form-control"
                                   name="amount" placeholder="{{ __('main.amount') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="bonusModalCommentInput">{{ __('main.bonus.transaction_batch_id') }}</label>
                            <input type="text" class="form-control" id="bonusModalCommentInput" name="batch_id" placeholder="{{ __('main.bonus.transaction_batch_id') }}">
                        </div>
                        <div class="form-group">
                            <label for="bonusModalSourceInput">{{ __('main.bonus.transaction_comment') }}</label>
                            <textarea rows="3" class="form-control" id="bonusModalSourceInput" name="source" placeholder="{{ __('main.bonus.transaction_comment') }}"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="wallet_id" value="">
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('main.close') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('main.send') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('load-scripts')
    <script>
        $('#bonusModal').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let walletId = button.data('wallet-id');
            let modal = $(this);

            modal.find('input[name="wallet_id"]').val(walletId);
        });
    </script>
@endpush